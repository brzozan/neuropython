#!/usr/bin/python

import random, sys

from neuro import *

testData    = [ ([0.0, 0.0], [1.0]),
                ([0.0, 1.0], [0.0]),
                ([1.0, 0.0], [0.0]),
                ([1.0, 1.0], [1.0]),
                ([0.5, 0.5], [0.0]),
                ([0.5, 0.0], [1.0]),
                ([1.0, 0.5], [1.0]),
                ([0.5, 1.0], [1.0]),
                ([0.0, 0.5], [1.0]),
              ]
testData    = [ ([0.0, 0.0], [1.0]),
                ([0.0, 1.0], [1.0]),
                ([1.0, 0.0], [1.0]),
                ([1.0, 1.0], [1.0]),
                ([0.5, 0.5], [1.0]),
                ([0.5, 0.0], [0.0]),
                ([0.5, 1.0], [0.0]),
                ([0.0, 0.5], [0.0]),
                ([1.0, 0.5], [0.0]),
              ]

step        = 0.6

layers      = [ Layer(2, 4, MomentumNeuron, outFunc = LOGISTIC, coeff = 2.0),
                Layer(4, 4, MomentumNeuron, outFunc = LOGISTIC, coeff = 3.0),
                Layer(4, 4, MomentumNeuron, outFunc = LOGISTIC, coeff = 3.0),
                Layer(4, 1, MomentumNeuron, outFunc = LOGISTIC, coeff = 5.0),
              ]

network     = Network(layers)

for i in range(0, 5000):
    random.shuffle(testData)
    
    for inputs, outputs in testData:
        network.learnSimple(inputs, outputs, step)
        
    step    = step * 0.999

    if i % 100 == 0:
        y   = 0.0
        for iy in range(0, 25):
            print
            x   = 0.0
            for ix in range(0, 50):
                out = network.output([x, y])

                if out[0] > 0.8:
                    sys.stdout.write('#')
                elif out[0] > 0.6:
                    sys.stdout.write('*')
                elif out[0] > 0.4:
                    sys.stdout.write('=')
                elif out[0] > 0.3:
                    sys.stdout.write(':')
                elif out[0] > 0.2:
                    sys.stdout.write('.')
                else:
                    sys.stdout.write(' ')

                x   = x + 0.02
            y   = y + 0.04
        print

print
for inputs, outputs in testData:
    print inputs, outputs, ':', network.output(inputs)
print
