#!/usr/bin/python

import random, sys

from neuro import *

def double2char(d):
    if d >= 0.9:
        return '#'
    elif d >= 0.8:
        return '*'
    elif d >= 0.6:
        return '='
    elif d >= 0.4:
        return ':'
    elif d >= 0.2:
        return ','
    elif d >= 0.1:
        return '.'
    else:
        return ' '

def char2double(c):
    if c == '#':
        return 1.0
    elif c == '*':
        return 0.8
    elif c == '=':
        return 0.6
    elif c == ':':
        return 0.5
    elif c == ',':
        return 0.3
    elif c == '.':
        return 0.1
    else:
        return 0.0

def draw(n):
    y   = 0.0
    for iy in xrange(0, 26):
        x   = 0.0
        for ix in xrange(0, 51):
            out = n.output([x, y])
            sys.stdout.write(double2char(out[0]))
            x   = x + 0.02
        y   = y + 0.04
        print
    print

def list2data(lines):
    res = []
    dy  = 1.0 / (len(lines) - 1)
    dx  = 1.0 / (len(lines[0]) - 1)
    y   = 0.0

    for l in lines:
        x   = 0.0
        for c in l:
            res.append(([x, y], [char2double(c)]))
            x   = x + dx
        y   = y + dy
        
    return res

testDataStr = [ '# #'   ,
                ' # '   ,
                '# #'   ,
              ]

testDataStr = [ '#   #' ,
                ' ### ' ,
                '  #  ' ,
                '  #  ' ,
                '  #  ' ,
              ]

testData    = list2data(testDataStr)

step        = 0.6

def buildLayers(nums):
    res     = []
    prev    = 2
    for n in nums:
        res.append(Layer(prev, n, MomentumNeuron, outFunc = LOGISTIC, coeff = 5.0))
        prev    = n
    res.append(Layer(prev, 1, MomentumNeuron, outFunc = LOGISTIC, coeff = 5.0))
    return res

layers      = buildLayers([10])
network     = Network(layers)

for i in range(0, 5001):
    random.shuffle(testData)
    
    for inputs, outputs in testData:
        network.learnSimple(inputs, outputs, step)
        
    step    = step * 0.999

    #if i % 1000 == 0:
    #    network.disturb(0.4)

    if i % 200 == 0:
        print '---------------- %5d -----------------' % i
        print
        draw(network)

print
for inputs, outputs in testData:
    print inputs, outputs, ':', network.output(inputs)
print
