#
# __init__.py   $Revision: 1.3 $  $Date: 2003/11/16 07:33:43 $  MB
#

from Neuron import *
from Layer  import *

__all__ = [ 'LINEAR', 'LOGISTIC', 'TANH',   # output functions
            'Neuron', 'MomentumNeuron',     # neuron classes
            'Layer',  'Network'             # network layer
          ]
