#
# Neuron.py $Revision: 1.3 $  $Date: 2003/11/16 07:33:43 $  MB
#

import math, random

def __outfuncLinear(e, coeff):
    return e * coeff

def __outfuncLinearDer(e, y, coeff):
    return coeff

def __outfuncLogistic(e, coeff):
    return 1.0 / (1.0 + math.exp(-coeff * e))

def __outfuncLogisticDer(e, y, coeff):
    return y * (1.0 - y)

def __outfuncTanh(e, coeff):
    return math.tanh(coeff * e)

def __outfuncTanhDer(e, y, coeff):
    return (1.0 + y) * (1.0 - y)

LINEAR      = (__outfuncLinear, __outfuncLinearDer)
LOGISTIC    = (__outfuncLogistic, __outfuncLogisticDer)
TANH        = (__outfuncTanh, __outfuncTanhDer)

# Simple neuron taught using simple algorithm:
# 
#   W(n+1)  = W(n) + dW, where  dW = step * outFunc' * delta * X
#
#   step    - learning step
#   outFunc'- derivative of output function
#   delta   - error found on neuron's output (desiredOutput - output)
#   X       - input signals
#
# Each neuron has inputsCount inputs + additional constant signal - bias.
# This signal makes it possible to get non-zero potential (e) when all
# inputs are 0.
#   
class Neuron:
    # Parameters:
    #   inputsCount - number of neuron inputs.
    #   
    #   outFunc     - tuple (outputFunction, outputFunctionDerivative). This
    #                 is the function transforming neuron potential (e) to
    #                 its output.
    #                 Predefined values are:
    #                   LINEAR      - y = coeff * e
    #                   LOGISTIC    - y = 1 / (1 + exp(-coeff * e))
    #                   TANH        - y = tanh(coeff * e)
    #                   
    #   bias        - value of the additional constant input signal.
    #   
    #   coeff       - coefficient used to count output function.
    def __init__(self, inputsCount, outFunc = LINEAR, bias=1.0, coeff = 1.0):
        self.inputsCount    = inputsCount + 1
        self.weights        = map(lambda i: 0.2 * random.random() - 0.1, xrange(0, self.inputsCount))
        
        self.bias           = bias
        self.coeff          = coeff

        self.outputFunction, self.outputFunctionDer = outFunc

    # Count neuron potential for given inputs set.
    def countE(self, inputs):
        # add bias signal to inputs
        inputs  = [self.bias] + inputs
        return reduce(lambda s, i: s + self.weights[i] * inputs[i], xrange(0, self.inputsCount), 0.0)

    def countOutput(self, e):
        return self.outputFunction(e, self.coeff)

    # Count neuron's output for given inputs set. output = outputFunction(e).
    def output(self, inputs):
        return self.outputFunction(self.countE(inputs), self.coeff)

    # Perform one simple learning step.
    # Parameters:
    #   inputs          - neuron inputs
    #   
    #   desired         - desired neuron output value
    #
    #   learningStep    - learning step :)
    #
    def learnSimple(self, inputs, desired, learningStep):
        e   = self.countE(inputs)
        y   = self.outputFunction(e, self.coeff)
        
        self.learn(inputs, e, y, desired - y, learningStep)

        return desired - y

    # Perform one learning step for given inputs assuming, that error
    # value on neuron output is delta. This is useful for backward propagation
    # networks.
    # 
    # Parameters:
    #   inputs          - neuron inputs
    #
    #   e               - neuron potential for these inputs
    #
    #   y               - neuron output value for these inputs
    #
    #   delta           - error made by this neuron
    #
    #   learningStep    - learning step
    #
    def learn(self, inputs, e, y, delta, learningStep):
        inputs          = [self.bias] + inputs
        k               = learningStep * self.outputFunctionDer(e, y, self.coeff) * delta
        for i in xrange(0, self.inputsCount):
            self.weights[i] = self.weights[i] + k * inputs[i]
            
    # Propagate delta value to the neurons from the previous network layer.
    # Used only in multi-layer networks.
    def propagateDeltas(self, delta):
        return map(lambda i: delta * self.weights[i], xrange(1, self.inputsCount))

    def propagateOneDelta(self, delta, index):
        return delta * self.weights[index + 1]

    # Disturb neuron's weights by value from range (-maxValue, maxValue).
    def disturb(self, maxValue):
        self.weights    = map(lambda i: self.weights[i] + 2.0 * maxValue * random.random() - maxValue, xrange(0, self.inputsCount))


# Neuron taught using momentum:
#   W(n+1)  = W(n) + dW + momentum * ( W(n) - W(n-1) )
class MomentumNeuron(Neuron):
    def __init__(self, inputsCount, outFunc = LINEAR, bias=1.0, coeff = 1.0, momentum = 0.6):
        Neuron.__init__(self, inputsCount, outFunc, bias, coeff)

        self.momentum           = momentum
        self.prevDeltaWeights   = [0.0] * self.inputsCount

    def learn(self, inputs, e, y, delta, learningStep):
        inputs          = [self.bias] + inputs
        k               = learningStep * self.outputFunctionDer(e, y, self.coeff) * delta

        for i in xrange(0, self.inputsCount):
            self.prevDeltaWeights[i]    = k * inputs[i] + self.momentum * self.prevDeltaWeights[i]
            self.weights[i]             = self.weights[i] + self.prevDeltaWeights[i]
















        
