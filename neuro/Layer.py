#
# Layer.py  $Revision: 1.2 $  $Date: 2003/11/16 07:33:43 $  MB
#

from Neuron import *

class Layer:
    def __init__(self, inputsCount, neuronsCount, neuronClass = Neuron, outFunc = LINEAR, bias = 1.0, coeff = 1.0):
        self.inputsCount    = inputsCount
        self.neuronsCount   = neuronsCount
        self.neurons        = map(lambda i: neuronClass(inputsCount, outFunc, bias, coeff), range(0, neuronsCount))

    def countEs(self, inputs):
        return map(lambda n: n.countE(inputs), self.neurons)

    def countOutputs(self, es):
        return map(lambda i: self.neurons[i].countOutput(es[i]), xrange(0, self.neuronsCount))

    def output(self, inputs):
        return self.countOutputs(self.countEs(inputs))

    def learnSimple(self, inputs, desired, learningStep):
        es      = self.countEs(inputs)
        ys      = self.countOutputs(es)
        deltas  = map(lambda i: desired[i] - ys[i], xrange(0, self.neuronsCount))

        self.learn(inputs, es, ys, deltas, learningStep)

    def learn(self, inputs, es, ys, deltas, learningStep):
        for i in xrange(0, self.neuronsCount):
            self.neurons[i].learn(inputs, es[i], ys[i], deltas[i], learningStep)

    def propagateDeltas(self, deltas):
        result      = [0.0] * self.inputsCount

        for i in xrange(0, self.inputsCount):
            for j in xrange(0, self.neuronsCount):
                result[i]   = result[i] + self.neurons[j].propagateOneDelta(deltas[j], i)

        return result

class Network:
    def __init__(self, layers):
        self.layers = layers

    def countOutputs(self, inputs):
        for l in self.layers:
            inputs  = l.output(inputs)
        return inputs

    def output(self, inputs):
        return self.countOutputs(inputs)

    def learnSimple(self, inputs, desired, learningStep):
        outputs = [inputs]
        ninputs = inputs
        
        for l in self.layers:
            ninputs = l.output(ninputs)
            outputs.append(ninputs)

        deltas  = map(lambda i: desired[i] - ninputs[i], xrange(0, len(desired)))

        for i in xrange(len(self.layers) - 1, -1, -1):
            ndeltas = self.layers[i].propagateDeltas(deltas)
            self.layers[i].learn(outputs[i], outputs[i + 1], outputs[i + 1], deltas, learningStep)
            deltas  = ndeltas




