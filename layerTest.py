#!/usr/bin/python

from neuro import *

testData    = [ ([0.0, 0.0, 0.0], [1.0, 1.0]),
                ([1.0, 0.0, 0.0], [0.0, 1.0]),
                ([0.0, 1.0, 0.0], [1.0, 0.0]),
                ([1.0, 1.0, 1.0], [0.0, 0.0])
              ]

step        = 0.6

layer       = Layer(3, 2, outFunc = LOGISTIC, coeff = 3.0)

for i in range(0, 500):
    for inputs, outputs in testData:
        layer.learnSimple(inputs, outputs, step)
        
    step    = step * 0.99

print
for inputs, outputs in testData:
    print inputs, outputs, ':', layer.output(inputs)
print
