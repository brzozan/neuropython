#!/usr/bin/python

import math, random
from neuro import *

def normalize(v):
    l   = math.sqrt(reduce(lambda s, x: s + x * x, v))
    if l > 0:
        return map(lambda x: x / l, v)
    else:
        return v

testData    = [([0.0, 0.0], 1.0), ([1.0, 1.0], 1.0), ([1.0, 0.0], 0.0), ([0.0, 1.0], 0.0)]
step        = 0.6
inputsCount = 2
hiddenCount = 4

neurons     = map(lambda i: Neuron(inputsCount, coeff = 2.0, outFunc = LOGISTIC), range(0, hiddenCount))
neurOut     = MomentumNeuron(hiddenCount, coeff = 3.0, outFunc = LOGISTIC)

for i in range(0, 10):
    for n in neurons:
        n.disturb(0.05)

    for j in range(0, 200):
        inputs, desired = random.choice(testData)
    
        for k in range(0, 5):
            outputs = map(lambda n: n.output(inputs), neurons)
            deltas  = neurOut.learnSimple(outputs, desired, step)

            for n in xrange(0, len(neurons)):
                neurons[n].learnSimple(inputs, outputs[n] + deltas[n], step)

        step    = step * 0.999

print
for inputs, desired in testData:
    outputs = map(lambda n: n.output(inputs), neurons)
    print inputs, desired, ':', neurOut.output(outputs)
print


