#!/usr/bin/python

from neuro import *

testData    = [([0.0, 0.0], 1.0), ([1.0, 0.0], 1.0), ([1.0, 1.0], 0.0), ([0.0, 1.0], 0.0)]
step        = 0.6
inputsCount = 2

neurons     = [ Neuron(inputsCount, coeff = 1.0, outFunc = LINEAR),
                Neuron(inputsCount, coeff = 3.0, outFunc = LOGISTIC),
                Neuron(inputsCount, coeff = 3.0, outFunc = TANH),
                
                MomentumNeuron(inputsCount, coeff = 1.0, outFunc = LINEAR),
                MomentumNeuron(inputsCount, coeff = 3.0, outFunc = LOGISTIC),
                MomentumNeuron(inputsCount, coeff = 3.0, outFunc = TANH)
              ]

for i in range(0, 500):
    for inputs, output in testData:
        for n in neurons:
            n.learnSimple(inputs, output, step)

    step    = step * 0.99

print
for n in neurons:
    for inputs, output in testData:
        print inputs, output, ': % 10.7f' % n.output(inputs)
    print

