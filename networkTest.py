#!/usr/bin/python

import random

from neuro import *

testData    = [ ([0.0, 0.0], [1.0]),
                ([1.0, 1.0], [1.0]),
                ([1.0, 0.0], [0.0]),
                ([0.0, 1.0], [0.0])
              ]

step        = 0.6

layers      = [ Layer(2, 2, MomentumNeuron, outFunc = LOGISTIC, coeff = 2.0),
                Layer(2, 1, MomentumNeuron, outFunc = LOGISTIC, coeff = 5.0)
              ]

network     = Network(layers)

for i in range(0, 1000):
    random.shuffle(testData)
    
    for inputs, outputs in testData:
        network.learnSimple(inputs, outputs, step)
        
    step    = step * 0.999

print
for inputs, outputs in testData:
    print inputs, outputs, ':', network.output(inputs)
print
